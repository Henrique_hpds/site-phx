document.addEventListener("DOMContentLoaded", function () {
  const slidesContainer = document.querySelector(".slides-container");
  const slides = document.querySelectorAll(".slide");
  const totalSlides = slides.length;
  let currentSlide = 0;

  function moveToSlide(slideIndex) {
    if (slideIndex < 0) slideIndex = totalSlides - 1;
    if (slideIndex >= totalSlides) slideIndex = 0;
    const offset = -slideIndex * 100;
    slidesContainer.style.transform = `translateX(${offset}%)`;
    currentSlide = slideIndex;
    updateIndicator(currentSlide);
  }

  function nextSlide() {
    moveToSlide(currentSlide + 1);
  }

  function prevSlide() {
    moveToSlide(currentSlide - 1);
  }

  setInterval(nextSlide, 3500);
  moveToSlide(currentSlide);

  // Inicialize o carrossel de imagens
  initializeImageCarousel();
});

function initializeImageCarousel() {
  const indicator = document.querySelector(".carousel-indicator");
  const images = document.querySelectorAll(".carousel img");

  images.forEach((image, index) => {
    const span = document.createElement("span");
    span.addEventListener("click", () => {
      setActiveImage(index);
    });
    indicator.appendChild(span);
  });

  setActiveImage(0);
}

function setActiveImage(index) {
  const images = document.querySelectorAll(".carousel img");
  const indicators = document.querySelectorAll(".carousel-indicator span");

  images.forEach((image) => {
    image.style.transform = `translateX(-${index * 100}%)`;
  });

  indicators.forEach((ind, i) => {
    if (i === index) {
      ind.classList.add("active");
    } else {
      ind.classList.remove("active");
    }
  });

  currentSlide = index;

}

function updateIndicator(index) {
  const indicators = document.querySelectorAll(".carousel-indicator span");

  indicators.forEach((ind, i) => {
    if (i === index) {
      ind.classList.add("active");
    } else {
      ind.classList.remove("active");
    }
  });
}
