# Site Phx


## Autores 

|Henrique Parede de Souza|Vinicius Patriarca Miranda Miguel|Luana Sayuri Amancio Aoki|
|-|-|
|||

Esse repositório está estruturado da seguinte maneira:

    - Pasta parte-compartilhadas:
        * global.css que é importado por todos os arquivos html para definir aspectos mais gerais da pagina, como fonte e tamanho de títulos, para assim garantir uma padronização;
        * headerFooter.js que define o header e o footer para todas as páginas e assim facilitar manutenção;

    - Pasta fotos: estão as imagens usadas no projeto;

    - Pasta fontes: estão as fontes usadas no projeto;
    
    - Pasta js: estão os arquivos .js do projeto;

    - Pasta style: estão os css das paginas html, com nomes equivalentes, vale lembrar que as diferentes páginas de projetos importam o mesmo css, o projetos.css;

    - Na raiz estão todos os html


Os arquivos css estão definidos da seguinte maneira:
    - No inicio estão definido aspectos gerais, como forma e disposição;
    - No fim estão definidos principalmente os tamanhos de cada parâmetro de cada elemento, tanto para celular, quanto para computadores, que estão definidos de maneiras diferentes.


É válido ressaltar que esse foi o primeiro site que nós criamos, então provavelmente há varios erros de estrutura, então podem xingar a gente a vontade.