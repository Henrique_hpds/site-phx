function createHeader() {
    const header = document.getElementById('header');

    // html do header
    header.innerHTML = `
        <nav class="limite">
        <div class="navbar">
          <div class="nav-container">
                <input class="checkbox" type="checkbox" name="" id="" />
                <div class="hamburger-lines">
                    <span class="line line1"></span>
                    <span class="line line2"></span>
                    <span class="line line3"></span>
                </div>  
                <div class="logo">
                    <a href="inicial.html" target="_self">     <img src="fotos/phoenix-2022-sem-fundo.png" alt="Logo Equipe Phoenix"></a>
                </div>
                <div class="menu-items">
                    <ul>
                        <li><a href="#">Equipe</a>
                            <ul class="dropdown">
                                <li><a href="equipe.html">Sobre</a></li>
                                <li><a href="membros.html">Membros</a></li>
                                <li><a href="historia.html">Historia</a></li>
                            </ul>
                        </li>

                        <li><a href="#">Projetos</a>
                            <ul class="dropdown">
                                <li><a href="autonomos.html">Autonomos</a></li>
                                <li><a href="feather.html">Feather</a></li>
                                <li><a href="insetos.html">Insetos</a></li>
                                <li><a href="sumo.html">Sumo</a></li>
                                    <ul class="subdrop">
                                        <!-- <li><a href="#">3kg</a></li>
                                        <li><a href="#">Mini</a></li> -->
                                    </ul>
                                <li><a href="treeking.html">Treeking</a></li>
                            </ul>
                        </li>

                        <li><a href="parcerias.html">Parceirias</a></li>

                        <li><a href="competicoes.html">Competicoes</a></li>
                    </ul>
              </div>
              <div id="redes-sociais">
                  <span id="instagram">
                      <a href="https://www.instagram.com/phoenixunicamp/" target="_blank" rel="external"><img src="fotos/redes-sociais/instagram-branco.png" alt="Instagram"></a>
                  </span>
                  <span id="youtube">
                      <a href="https://www.youtube.com/@PhoenixUnicamp/" target="_blank" rel="external"><img src="fotos/redes-sociais/youtube-branco.png" alt="Youtube"></a>
                  </span>
              </div>
          </div>
        </div>
      </nav>

    `;

    var sheet = document.createElement('style');

    // css do header
    sheet.innerHTML = `


    /* Estilos para celulares */
    @media (orientation: portrait) {
        .navbar {
            width: 100%;
            background: black;
        }
        
        .nav-container {
            display: flex;
            justify-content: space-between;
            align-items: center;
            height: 62px;
        }
        
        .navbar .menu-items {
            display: flex;
        }
        
        .navbar .nav-container li {
            list-style: none;
        }
        
        .navbar .nav-container a {
            text-decoration: none;
            color: #ffff;
            font-weight: 500;
            font-size: 1.2rem;
            padding: 0.7rem;
        }
        
        .navbar .nav-container a:hover{
            font-weight: bolder;
        }
        
        .nav-container {
            display: block;
            position: relative;
            height: 85px;
        }
        
        .nav-container .checkbox {
            position: absolute;
            background-color: green;
            display: block;
            height: 32px;
            width: 32px;
            top: 20px;
            left: 20px;
            z-index: 5;
            opacity: 0;
            cursor: pointer;
        }
        
        .nav-container .hamburger-lines {
            display: block;
            height: 26px;
            width: 32px;
            position: absolute;
            top: 32px;
            left: 20px;
            z-index: 2;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
        }
        
        .nav-container .hamburger-lines .line {
            display: block;
            height: 4px;
            width: 100%;
            border-radius: 10px;
            background: #ffffff;
        }
        
        .nav-container .hamburger-lines .line1 {
            transform-origin: 0% 0%;
            transition: transform 0.4s ease-in-out;
        }
        
        .nav-container .hamburger-lines .line2 {
            transition: transform 0.2s ease-in-out;
        }
        
        .nav-container .hamburger-lines .line3 {
            transform-origin: 0% 100%;
            transition: transform 0.4s ease-in-out;
        }
        
        .navbar .menu-items {
            font-family: 'BLANKA', sans-serif;
            color: #ffffff;
            padding-top: 20px;
            background: black;
            height: 100vh;
            width: 100%;
            transform: translate(-150%);
            display: flex;
            flex-direction: column;
            margin-top: 80px;
            margin-left: -40px;
            padding-left: 50px;
            transition: transform 0.5s ease-in-out;
            text-align: center;
            position: absolute;
            z-index: 3;
        }
        
        .navbar .menu-items li {
            margin-bottom: 1.2rem;
            font-size: 1.5rem;
            font-weight: 500;
        }
        
        
        .menu-items ul li {
            text-align: left;
        }

        .logo img{
            height: 50px;
            position: absolute;
            top: 19px;
            right: 19px;
        }
        
        .nav-container input[type="checkbox"]:checked ~ .menu-items {
            transform: translateX(0);
        }
        
        .nav-container input[type="checkbox"]:checked ~ .hamburger-lines .line1 {
            transform: rotate(45deg);
        }
        
        .nav-container input[type="checkbox"]:checked ~ .hamburger-lines .line2 {
            transform: scaleY(0);
        }
        
        .nav-container input[type="checkbox"]:checked ~ .hamburger-lines .line3 {
            transform: rotate(-45deg);
        }
        
        .nav-container input[type="checkbox"]:checked ~ .logo{
            display: none;
        }

        header #redes-sociais{
            position: absolute;
            justify-content: right;
        }

        header #redes-sociais img{
            height: 30px;
        }
    }

    /* Estilos para computadores */
    @media (orientation: landscape) {
        
        
        .nav-container{
            display: flex;
            align-items: center;
            padding: 30px 50px;
        }

        .navbar {
            width: 100%;
            background: black;
            align-items: center; /* Alinha verticalmente os itens internos ao centro */

        }
        
        .havbar a:hover{
            color: var(--phx);
        }

        .hamburger-lines{
            display: none;
        }


        .logo img{
            height: 100px;
            position: left;
        }
        
        .logo img:hover{
            transform: scale(1.05);
            transition: transform 0.4s ease;
        }

        .checkbox{
            display: none;
        }

        
        .menu-items ul {
            font-family: 'BLANKA', sans-serif;
            padding: 0;
            margin: 0;
        }

        .menu-items{
            text-align: right;
            flex: 1;
        }

        .menu-items a {
            font-size: 20px;
            color: white;
            display: block;
            padding-left: 2.1vw;
            padding-right: 2.1vw;
            padding-bottom: 1.1vh;
            padding-top: 1.1vh;
            text-decoration: none;
        }

        .menu-items a:hover {
            color: var(--phx);
            transform: scale(1.1);
            transition: transform 0.4s ease;
        }
        
        .menu-items ul li {
            display: inline-block;
            position: relative;
        }
        
        .menu-items ul li:hover ul.dropdown {
            display: block;
        }
        
        .menu-items ul li ul.dropdown li {
            display: block;
            background: black;
            text-align: center;
            flex: 1;
        }
        .menu-items ul li ul.dropdown {
            z-index: 10;
            display: none;
            position: absolute;
            min-width: 7vw;
        }

        header #redes-sociais{
            margin-left: auto; /* Empurra as redes sociais para a direita */
        }

        header #redes-sociais img{
            padding-left: 1vw;
            height: 50px;
        }

        header #redes-sociais img:hover{
            transform: scale(1.1);
            transition: transform 0.4s ease;
        }
    }
        `;

    document.body.appendChild(sheet);
}

// Criar o conteúdo do footer
function createFooter() {
    const footer = document.getElementById('footer');
    footer.innerHTML = `
        <div>
            <div id="logo">
                <img src="fotos/phoenix-2022-com-nome.png" alt="Logo Equipe Phoenix">
            </div>
            <div id="endereco">
                <h1>Endereco</h1>
                <p>R. Bernardo Sayão 100<br>
                Cidade Universitária<br>
                Campinas - SP, 13083-866 </p>
            </div>
            <div id="contato">
                <h1>Contato</h1>
                <p>Telefone: <span id="telefone"></span><br>
                E-mail: rephoenixunicamp@gmail.com<br>
                Capitão: Giovani Moreira da Silva</p>

            </div>
            <div id="imagem-unicamp">
                <img src="fotos/logo_unicamp_branco.png" alt="Logo Unicamp">
            </div>

            <div id="copyright">
                <p>&copy; <span id="ano"></span> Equipe Phoenix</p>
            </div>
        </div>

        <script src="jquery.js"></script>
        <script src="jquery.csv.js"></script>
        <script src="jquery.csv.min.js"></script>
        <script src='dados/leitura.js'></script>
    `;

    var sheet = document.createElement('style');
    sheet.innerHTML = `
        footer{
            display: flex;
            background-color: black;
            color: white;
            align-items: center;
            justify-content: center;
        }

        footer div div{
            display: inline-block;
            margin: 2.5vw;
        }

        footer div div h1{
            font-family: 'BLANKA', sans-serif;
            font-size: 1.5vw;
        }

        footer div div p{
            font-size: 0.9vw;
        }

        footer #logo img{
            width: 15vw;
        }

        footer #imagem-unicamp img{
            width: 7vw;
        }

        footer #copyright{
            display: flex;
            align-items: center;
            justify-content: center;
            font-weight: bold;
            margin: 0;
            padding: 0;
        }
    `;

    document.body.appendChild(sheet);
}

function atualizarCopyright() {
    const anoAtual = document.getElementById('ano');
    anoAtual.innerHTML = new Date().getFullYear();
}

// Chamar as funções para criar o header e footer
createHeader();
createFooter();
atualizarCopyright();